package schema

import "gorm.io/gorm"

type Transaction struct {
	gorm.Model
	UserID uint `gorm:"index"`
	User   User
	Delta  int `gorm:"type:int"` // Positive for credit, negative for debit
}
