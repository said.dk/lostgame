package schema

import "gorm.io/gorm"

// This is a toy example, in real life we would have more complicated structure:
// For instance, a single user can operate using different wallets (and currencies).

// User is a user of the system.
type User struct {
	gorm.Model
	Email   string `gorm:"unique"`
	Balance int    `gorm:"type:int"` // Some denormalization for performance and convenience reasons
}
