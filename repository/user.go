package repository

import (
	"fmt"

	"gitlab.com/said.dk/lostgame/config"
	"gitlab.com/said.dk/lostgame/core"
	"gitlab.com/said.dk/lostgame/schema"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

var InsufficientBalanceError = core.NewGenericError("insufficient balance", "INSUFFICIENT_BALANCE")

// UserRepository is a repository for Users.
type UserRepository struct {
	db *gorm.DB
}

// NewUserRepository creates a new UserRepository.
func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{db}
}

// GetByID returns the user with the given ID.
func (ur *UserRepository) GetByID(id uint) (*schema.User, error) {
	var user schema.User
	if err := ur.db.First(&user, id).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

// CreateTestUser creates a test user with 1000 coins as balance.
func (ur *UserRepository) CreateTestUser() error {
	// test user can only be created in local environment
	if !config.Environment.IsLocal() {
		return nil
	}
	user := &schema.User{
		Email:   "test@test.com",
		Balance: 1000,
	}
	if err := ur.db.Create(user).Error; err != nil {
		return err
	}
	fmt.Printf("\ntest user created with ID: %d\n", user.ID)
	return nil
}

// GetBalanceByID returns the balance of the user with the given ID.
// It's a dirty-read method, it doesn't lock the row, nor does it start a transaction.
func (ur *UserRepository) GetBalanceByID(id uint) (int, error) {
	var user schema.User
	if err := ur.db.First(&user, id).Error; err != nil {
		return 0, err
	}
	return user.Balance, nil
}

// UpdateBalance updates the balance of the user with the given ID by the given delta.
// It uses a transaction to ensure the consistency of the data.
// Parallel updates to the same user's balance will be serialized.
func (ur *UserRepository) UpdateBalance(id uint, delta int) (*schema.Transaction, error) {
	var trans *schema.Transaction
	err := ur.db.Transaction(func(tx *gorm.DB) error {
		var user schema.User
		if err := tx.Clauses(clause.Locking{Strength: "UPDATE"}).First(&user, id).Error; err != nil {
			return err
		}
		if user.Balance+delta < 0 {
			return InsufficientBalanceError
		}
		user.Balance += delta
		if err := tx.Save(&user).Error; err != nil {
			return err
		}
		trans = &schema.Transaction{
			UserID: id,
			Delta:  delta,
		}
		if err := tx.Create(trans).Error; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	if err := ur.db.Preload("User").First(trans, trans.ID).Error; err != nil {
		return nil, err
	}
	return trans, nil
}
