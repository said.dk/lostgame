package repository

import "gorm.io/gorm"

// Repositories is a collection of repositories.
type Repositories struct {
	Users *UserRepository
	// some other repositories if needed
}

// NewRepositories creates an instance of Repositories.
func NewRepositories(db *gorm.DB) *Repositories {
	return &Repositories{
		Users: NewUserRepository(db),
	}
}
