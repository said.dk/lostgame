# lostgame

Test project that demonstrates server implementation in Golang, using Fiber and Gorm

## To run the project

```bash
docker compose up -d --build
```

If you run the project in the LOCAL environment, a new test user will be created.

In the logs you will see the user's new ID message, which might look as follows:

```text
test user created with ID: 1
```

To query a balance:

```bash
curl --location 'localhost:3000/open-api-games/v1/games-processor' \
--header 'Content-Type: application/json' \
--data '{
    "api": "balance",
    "data": {
        "userID": 1
    }
}'
```

To debit:

```bash
curl --location 'localhost:3000/open-api-games/v1/games-processor' \
--header 'Content-Type: application/json' \
--data '{
    "api": "debit",
    "data": {
        "userID": 1,
        "amount": 420
    }
}'
```

To credit:

```bash
curl --location 'localhost:3000/open-api-games/v1/games-processor' \
--header 'Content-Type: application/json' \
--data '{
    "api": "credit",
    "data": {
        "userID": 1,
        "amount": 69
    }
}'
```
