FROM golang:1.22.1-alpine AS builder
WORKDIR /lostgame
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN GOOS=linux go build -o /lostgame

FROM alpine:3.19.1
WORKDIR /
COPY --from=builder /lostgame .
CMD ["./lostgame"]
