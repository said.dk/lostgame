package commands

import (
	"encoding/json"

	"gitlab.com/said.dk/lostgame/repository"
)

// Command is an interface that all commands must implement
type Command interface {
	// Parse parses the json.RawMessage into the command struct
	Parse(json.RawMessage) error

	// Execute executes the command and returns the response
	Execute(repos *repository.Repositories) (interface{}, error)
}
