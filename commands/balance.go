package commands

import (
	"encoding/json"
	"fmt"

	"gitlab.com/said.dk/lostgame/repository"
)

var BalanceCommandName = "balance"

// BalanceCommand is a command that returns the balance of a user
type BalanceCommand struct {
	UserID uint `json:"userId"`
}

func (b *BalanceCommand) Parse(data json.RawMessage) error {
	b.UserID = 0
	if err := json.Unmarshal(data, &b); err != nil {
		return err
	}
	return nil
}

func (b *BalanceCommand) Execute(repos *repository.Repositories) (interface{}, error) {
	fmt.Print("BalanceCammand called") // replace with zerolog
	user, err := repos.Users.GetByID(b.UserID)
	if err != nil {
		return nil, err
	}
	return user, nil
}
