package commands

import (
	"encoding/json"
	"fmt"

	"gitlab.com/said.dk/lostgame/repository"
)

var DebitCommandName = "debit"

// DebitCommand is a command that withdraws a debit from users' balance
type DebitCommand struct {
	UserID uint `json:"userId"`
	Amount uint `json:"amount"`
}

func (d *DebitCommand) Parse(data json.RawMessage) error {
	d.UserID = 0
	d.Amount = 0
	if err := json.Unmarshal(data, &d); err != nil {
		return err
	}
	return nil
}

func (d *DebitCommand) Execute(repos *repository.Repositories) (interface{}, error) {
	fmt.Print("DebitCommand called")
	trans, err := repos.Users.UpdateBalance(d.UserID, -int(d.Amount))
	if err != nil {
		return nil, err
	}
	return trans, nil
}
