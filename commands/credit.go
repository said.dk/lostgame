package commands

import (
	"encoding/json"
	"fmt"

	"gitlab.com/said.dk/lostgame/repository"
)

var CreditCommandName = "credit"

// CreditCommand is a command that applies a credit to a user's balance
type CreditCommand struct {
	UserID uint `json:"userId"`
	Amount uint `json:"amount"`
}

func (w *CreditCommand) Parse(data json.RawMessage) error {
	w.UserID = 0
	w.Amount = 0
	if err := json.Unmarshal(data, &w); err != nil {
		return err
	}
	return nil
}

func (w *CreditCommand) Execute(repos *repository.Repositories) (interface{}, error) {
	fmt.Print("CreditCommand called")
	trans, err := repos.Users.UpdateBalance(w.UserID, int(w.Amount))
	if err != nil {
		return nil, err
	}
	return trans, nil
}
