package main

import (
	"log"

	"github.com/gofiber/fiber/v3"
	"github.com/gofiber/fiber/v3/middleware/cors"
	"github.com/gofiber/fiber/v3/middleware/logger"
	"gitlab.com/said.dk/lostgame/config"
	"gitlab.com/said.dk/lostgame/db"
	"gitlab.com/said.dk/lostgame/handlers"
)

func main() {
	app := fiber.New()
	app.Use(logger.New())
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept, Authorization",
		AllowMethods: "GET, POST", // TODO: reconsider allowed methods
	}))
	gormDB, err := db.New()
	if err != nil {
		log.Fatal(err) // TODO: for now any DB related error is fatal, but we need to reconsider this
	}
	db.AutoMigrate(gormDB)
	games := handlers.NewGameProcessor(gormDB)
	if config.Environment.IsLocal() {
		games.Repos.Users.CreateTestUser()
		// other logic that's suposed to be executed only in the local env
	}
	games.Register(app.Group("/open-api-games/v1"))
	log.Fatal(app.Listen(":" + config.Port))
}
