package handlers

import (
	"encoding/json"
	"errors"

	"github.com/gofiber/fiber/v3"
	"gitlab.com/said.dk/lostgame/core"
	"gitlab.com/said.dk/lostgame/dispatcher"
	"gitlab.com/said.dk/lostgame/repository"
	"gorm.io/gorm"
)

// GamesProcessor is a handler for processing games.
type GamesProcessor struct {
	Repos      *repository.Repositories
	dispatcher *dispatcher.Dispatcher
}

// NewGameProcessor creates a new GamesProcessor.
func NewGameProcessor(db *gorm.DB) *GamesProcessor {
	repos := repository.NewRepositories(db)
	return &GamesProcessor{
		Repos:      repos,
		dispatcher: dispatcher.New(repos),
	}
}

// Register registers the GamesProcessor to the given router.
func (gp *GamesProcessor) Register(router fiber.Router) {
	router.Post("/games-processor", func(c fiber.Ctx) error {
		body := struct {
			Api  string          `json:"api"`
			Data json.RawMessage `json:"data"`
		}{}
		if err := c.Bind().Body(&body); err != nil {
			return err
		}
		response, err := gp.dispatcher.Dispatch(body.Api, body.Data)
		ge := core.ErrorToGenericError(err)
		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"api":       body.Api,
			"isSuccess": errors.Is(err, core.NoErrorsError),
			"error":     ge.Message(),
			"errorMsg":  ge.Code(), // this is a little bit counterintuitive, but this is how docs wanted it to be
			"data":      response,
		})
	})
}
