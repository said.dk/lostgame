package core

import (
	"fmt"
)

var (
	SignNotProvidedError     = NewGenericError("sign is not provided, please provide a valid signature", "SIGN_NOT_PROVIDED")
	InvalidSignError         = NewGenericError("got invalid signature, please check your request and try again", "INVALID_SIGN")
	InsufficientBalanceError = NewGenericError("insufficient balance, operation is rejected", "INSUFFICIENT_BALANCE")
	NoErrorsError            = NewGenericError("", "NO_ERRORS") // :D
)

// GenericError is a generic error type that can be used to represent any error.
type GenericError struct {
	message string
	code    string
}

// Error returns the error message.
func (e *GenericError) Error() string {
	return fmt.Sprintf("%s: %s", e.code, e.message)
}

// Message returns the error message.
func (e *GenericError) Message() string {
	return e.message
}

// Code returns the error code (string).
func (e *GenericError) Code() string {
	return e.code
}

// NewGenericError creates a new GenericError.
func NewGenericError(message, code string) *GenericError {
	return &GenericError{
		message: message,
		code:    code,
	}
}

type unknownCommandError struct {
	GenericError
	CommandName string
}

// NewUnknownCommandError creates a new unknownCommandError.
func NewUnknownCommandError(commandName string) *unknownCommandError {
	return &unknownCommandError{
		GenericError: GenericError{
			message: fmt.Sprintf("command %s is not recognized", commandName),
			code:    "UNKNOWN_COMMAND",
		},
		CommandName: commandName,
	}
}

type internalError struct {
	GenericError
}

// NewInternalError creates a new internalError.
func NewInternalError(err error) *internalError {
	return &internalError{
		GenericError: GenericError{
			message: err.Error(),
			code:    "INTERNAL_ERROR",
		},
	}
}

// ErrorToGenericError converts an error to a GenericError.
func ErrorToGenericError(err error) *GenericError {
	if err == nil {
		return NoErrorsError
	}
	if genericErr, ok := err.(*GenericError); ok {
		return genericErr
	}
	return &NewInternalError(err).GenericError
}
