package dispatcher

import (
	"encoding/json"

	"gitlab.com/said.dk/lostgame/commands"
	"gitlab.com/said.dk/lostgame/core"
	"gitlab.com/said.dk/lostgame/repository"
)

// Dispatcher is a struct that dispatches commands to the appropriate handler
type Dispatcher struct {
	// TODO: add a pool of commands here, instead of creating new ones all the time
	repos *repository.Repositories
}

// New creates a new Dispatcher
func New(repos *repository.Repositories) *Dispatcher {
	return &Dispatcher{
		repos: repos,
	}
}

func (d *Dispatcher) lookupCommand(command string) (commands.Command, error) {
	switch command {
	case commands.BalanceCommandName:
		return &commands.BalanceCommand{}, nil
	case commands.DebitCommandName:
		return &commands.DebitCommand{}, nil
	case commands.CreditCommandName:
		return &commands.CreditCommand{}, nil
	default:
		return nil, core.NewUnknownCommandError(command)
	}
}

// Dispatch dispatches a command to the appropriate handler
func (d *Dispatcher) Dispatch(command string, data json.RawMessage) (interface{}, error) {
	cmd, err := d.lookupCommand(command)
	if err != nil {
		return nil, err
	}
	if err := cmd.Parse(data); err != nil {
		return nil, err
	}
	return cmd.Execute(d.repos)
}
