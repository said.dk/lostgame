package environment

import (
	"fmt"
	"strings"
)

const (
	LOCAL = Environment("LOCAL")
	DEV   = Environment("DEV")
	STAGE = Environment("STAGE")
	PROD  = Environment("PROD")
)

type Environment string

func New(env string) Environment {
	env = strings.ToUpper(env)
	switch env {
	case LOCAL.String():
		return LOCAL
	case DEV.String():
		return DEV
	case STAGE.String():
		return STAGE
	case PROD.String():
		return PROD
	default:
		fmt.Printf("Environment %s is not standard. Standard environments are %s.\n", env, []Environment{LOCAL, DEV, STAGE, PROD})
		return Environment(env)
	}
}

func (e Environment) IsLocal() bool {
	return e == LOCAL
}

func (e Environment) IsDev() bool {
	return e == DEV
}

func (e Environment) IsStage() bool {
	return e == STAGE
}

func (e Environment) IsProd() bool {
	return e == PROD
}

func (e Environment) IsStandard() bool {
	return e == LOCAL || e == DEV || e == STAGE || e == PROD
}

func (e Environment) String() string {
	return string(e)
}
