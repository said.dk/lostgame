package config

import (
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/said.dk/lostgame/config/environment"
)

var (
	Environment environment.Environment
	Port        string
	PostgresDsn string
)

func init() {
	godotenv.Load()
	Environment = environment.New(os.Getenv("ENVIRONMENT"))
	Port = os.Getenv("PORT")
	PostgresDsn = os.Getenv("POSTGRES_DSN")
}
